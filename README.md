# Task Feedback

The test can be found in the ``/app`` directory.

# Additional Tools Used

- [Apollo Client](https://www.apollographql.com/)

# Improvements / Retrospective Review

- Would have used Cypress for E2E testing
- I decided to use useQuery hook instead of using the old way of query graph
- 100% test coverage
- create a better Error handler
- I would have implemented typescript to better type definitions
- better styling
- I would have preffered to use React Testing Library for test. I find it a bit more streamlined for testing 
react components, especially with the Apollo mock, it seems to handle it bit better and easier to debug
