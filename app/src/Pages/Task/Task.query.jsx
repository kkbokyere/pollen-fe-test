import gql from 'graphql-tag';

export const ALL_REWARDS = gql`
    query allRewards($page: Int, $campaign_id: ID, $quantity: Int, $points: Int, $capPerAmbassador: Int ) {
        allRewards(page: $page, filter: { campaign_id:$campaign_id, quantity: $quantity, points: $points, capPerAmbassador: $capPerAmbassador }) {
            name
            description
            image
            quantity
            points
            campaign_id
            capPerAmbassador
        }
    }
`;
