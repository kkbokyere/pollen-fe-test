import React, {useState} from 'react';
import { useQuery } from '@apollo/client';
import RewardsList from "./components/RewardsList/RewardsList.jsx";
import {ALL_REWARDS} from "./Task.query.jsx";
import Filters from "./components/Filters/Filters.jsx";
import './Task.scss';
const Task = () => {
    const [filters, setFilters] = useState({});
    const { loading, error, data } = useQuery(ALL_REWARDS, {
        variables: { campaign_id: "001", ...filters}
    });
    const updateFilters = (filters) => {
        setFilters(filters);
    };
    const resetFilters = () => {
        setFilters({})
    };
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    return (
        <div className="task">
            <Filters handleOnUpdate={updateFilters} handleResetFilters={resetFilters} selectedFilters={filters}/>
            <RewardsList rewards={data.allRewards}/>
        </div>
        )
};

export default Task;
