import React from 'react';
import { shallow } from 'enzyme';
import Filters from "./Filters";

describe('<Filters />', () => {
    let component;

    it('renders with snapshot', () => {
        component = shallow(<Filters />);
        expect(component).toMatchSnapshot();
    });


    it('should update when submit is pressed', () => {
        const updateMockFn = jest.fn();
        component = shallow(<Filters handleOnUpdate={updateMockFn}/>);
        component.find('#quantity-input').simulate('change', { currentTarget: { value: 1 } });
        component.find('#submit-btn').simulate('click');
        expect(updateMockFn).toBeCalled();
    });

});
