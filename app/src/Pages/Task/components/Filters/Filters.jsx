import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import Input from "../Input/Input.jsx";
import './Filters.scss'
const Filters = ({ handleOnUpdate, handleResetFilters, selectedFilters = {}}) => {
    const [filters, setFilters] = useState({});
    const handleOnChange = (e) => {
        setFilters({...filters, [e.currentTarget.name]: e.currentTarget.value})
    };
    const handleSubmit = () => {
        handleOnUpdate(filters)
    };
    return (<div className="filters">
        <h2>Filters</h2>

        <div className="filters__input">
            <Input id='quantity-input' label="Quantity" type="text" name="quantity" onChange={handleOnChange} />
            <Input label="Points" type="number" name="points" onChange={handleOnChange} />
            <Input label="CapPerAmbassador" type="number" name="capPerAmbassador" onChange={handleOnChange} />
        </div>
        <div className="filters__buttons">
            <button id="submit-btn" onClick={handleSubmit}>submit</button>
            <button id="reset-btn"  onClick={handleResetFilters}>reset</button>
        </div>
        <div className="filters__selected">
            Selected filters:
            {selectedFilters && Object.keys(selectedFilters).map((title, i) => {
            return <p>{title}: {selectedFilters[title]}</p>
        })}</div>
    </div>)
};

Filters.propTypes = {
    handleOnUpdate: PropTypes.func,
    handleResetFilters: PropTypes.func
};

export default Filters;
