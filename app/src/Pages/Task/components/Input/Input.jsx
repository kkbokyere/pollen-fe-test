import React from 'react'
import './Input.scss'
const Input = ({label, ...props}) => {
    console.log(props)
    return(
        <label className="input-label">
            {label}
            <input className="input-field" {...props}/>
        </label>
    )
};

export default Input;
