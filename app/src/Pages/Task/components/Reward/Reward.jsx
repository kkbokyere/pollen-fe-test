import React from 'react'
import PropTypes from 'prop-types';
import './Reward.scss'
const Reward = ({ data }) => {
    const { image, name, description, campaign_id, quantity, capPerAmbassador, points } = data;
    return(
        <div className="reward">
            <img className="reward__img" src={image} alt={name}/>
            <h3 className="reward__name">{name}</h3>
            <div className="reward__desc">
                {description}
            </div>
            <div>
                <ul className="reward__meta">
                    <li>Quantity: <b>{quantity}</b></li>
                    <li>capPerAmbassador: <b>{capPerAmbassador}</b></li>
                    <li>Points: <b>{points}</b></li>
                </ul>
            </div>
        </div>
    )
};

Reward.propTypes = {
    data: PropTypes.shape({
        image: PropTypes.string,
        name: PropTypes.string,
        description: PropTypes.string,
        capPerAmbassador: PropTypes.number,
        quantity: PropTypes.number,
        points: PropTypes.number,
    }),
};

export default Reward;
