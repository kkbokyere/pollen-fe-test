import React from 'react';
import { shallow } from 'enzyme';

import Reward from "./Reward.jsx";

describe('<Reward />', () => {
    let component;

    it('renders with snapshot', () => {
        component = shallow(<Reward data={{ image: 'imgpath', name: 'test', description: 'test'}} />);
        expect(component).toMatchSnapshot();
    });

});
