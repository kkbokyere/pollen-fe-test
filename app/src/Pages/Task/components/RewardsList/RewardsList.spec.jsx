import React from 'react';
import { shallow, mount } from 'enzyme';

import RewardsList from "./RewardsList.jsx";

const mocks = [{
    "name": "Campaign X Flag",
    "description": "Stand out in a crowd with this limited edition Campaign X festival flag",
    "image": "https://d3mivsidb2c4zk.cloudfront.net/client/reward_images/1519118356.png",
    "quantity": 100,
    "points": 500,
    "capPerAmbassador": 1
},
    {
        "name": "Free Drink Voucher",
        "description": "Get a free drink on us!",
        "image": "https://d3mivsidb2c4zk.cloudfront.net/client/reward_images/1519128358.jpg",
        "quantity": 0,
        "points": 1000,
        "capPerAmbassador": 5
    }];

describe('<RewardsList />', () => {
    let component;

    it('renders with snapshot', () => {
        component = shallow(<RewardsList />);
        expect(component).toMatchSnapshot();
    });

    it('renders with list with results', () => {
        component = shallow(<RewardsList rewards={mocks}/>);
        expect(component.find('.reward-list').children()).toHaveLength(2);
    });
});
