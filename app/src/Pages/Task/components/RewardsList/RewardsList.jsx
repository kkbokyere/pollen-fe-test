import React from 'react'
import Reward from "../Reward/Reward.jsx";
import './RewardsList.scss'
const RewardsList = ({ rewards }) => {
    return(
        <div>
            <h3>Rewards</h3>
            <div className="reward-list">
                {rewards && rewards.map((item, i) => <Reward key={`reward-${i}`} data={item}/>)}
            </div>
        </div>
    )
};

export default RewardsList;
