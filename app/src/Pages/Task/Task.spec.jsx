import React from 'react';
import { shallow, mount } from 'enzyme';
import { MockedProvider } from "@apollo/client/testing";
import { act } from 'react-dom/test-utils';

import Task from './Task.jsx';
import {ALL_REWARDS} from "./Task.query";
import RewardsList from "./components/RewardsList/RewardsList";

describe('<Task />', () => {
    let component;
    const mocks = [{
        request: {
            query: ALL_REWARDS,
            variables: {"campaign_id":"001"}
        },
        result: () => {
            return ({
                data: {
                    allRewards: [{
                        "name": "Campaign X Flag",
                        "description": "Stand out in a crowd with this limited edition Campaign X festival flag",
                        "image": "https://d3mivsidb2c4zk.cloudfront.net/client/reward_images/1519118356.png",
                        "quantity": 100,
                        "points": 500,
                        "capPerAmbassador": 1
                    },
                        {
                            "name": "Free Drink Voucher",
                            "description": "Get a free drink on us!",
                            "image": "https://d3mivsidb2c4zk.cloudfront.net/client/reward_images/1519128358.jpg",
                            "quantity": 0,
                            "points": 1000,
                            "capPerAmbassador": 5
                        }]
                },
            })
        }
    }];
    beforeEach( async () => {
        component = shallow(<MockedProvider mocks={mocks}>
            <Task />
        </MockedProvider>);

        await new Promise(resolve => setTimeout(resolve, 0)); // wait for response
    });

    it('renders with snapshot', () => {
        expect(component).toMatchSnapshot();
    });

});
