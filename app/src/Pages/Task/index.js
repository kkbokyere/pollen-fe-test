import React from 'react'
import './styles/index.scss'
import Task from './Task.jsx';
import { ApolloProvider } from '@apollo/client';
import {client} from "../../Apollo/Apollo.jsx";

const ApolloClient = () => (
    <ApolloProvider client={client}>
        <Task/>
    </ApolloProvider>
);
export default ApolloClient;
