# Code Review

Overall, although this may work, you just need to follow some consistent functional programming techniques.
Also there is some inconsistency with the way you're using ES6, there is some parts that will benefit from some simple 
ES6 methods / syntax. Also assuming were using React 16, i suggest to stick to a functional approach using hooks.

## /actions

### index.js

#### Must Have
- No need use the explicit ``Object()`` in the return value from each function. 
This is redundant as you can just simplify it like this:
``const functionA = () => ({prop: 'val'})``

- you can use the spread operator on ``action`` param, to spread the props in the object:
``const functionA = (action) => ({type:'', ...action})``

#### Nice To Have
- consider if its worth storing the action types as constants in another file, then you can export and reuse them in the reducer.

## /components

### /Basket

#### Must Have
- No need use the explicit ``Object()`` in the return value of ``mapStateToProps``
- no need to use the extra lodash omit function to get the basket array from props. Just use destructuring to take the basket out e.g:
``BasketComponent = ({ basket }) => {``
- use the filter method on the basket to remove any items that do not have a quantity less than 1
- no need to return a blank string or do the check for an item and quantity in the basket map function, if you filter.
- write some more test to check multiple basket card items
- why is the Basket.scss imported but not used in this component

#### Nice To Have
- Separate the stateful redux logic from the functional UI component. I suggest creating a container file with the redux 
logic. This makes it easier to test the Basket component independent of any state.

### /BasketCard

#### Must Have
- Use a functional approach rather than a class based approach. stay consistent with rest of code plus there's no need.
- use function declaration for ``function getCardCost(price, quantity)``
- reassigning the cost variable in getCardCost is redundant. No need for the cost variable, just ``return (price * quantity)``
- use functional declaration for ``handleClick``. assuming you change this component to a functional one, you can access props from the params
- Need to write more tests to cover the cost output is correct based on a given price and quantity. not just snapshots
- should the handleClick be on the button instead of the div? semantically, that's incorrect, add the click to the button.

#### Nice To Have
- if you create a container file with the redux logic to dispatch actions, then you can separate the redux logic away.
This also makes the component more easier to test.

### /TicketCard

#### Must Have
- Use a functional approach rather than a class based approach. stay consistent with rest of code plus there's no need.
- Consider using Hooks to manage local state ``const [active, setActive] = useState()``

#### Nice To Have
- if you create a container file with the redux logic to dispatch actions, then you can separate the redux logic away.
This also makes the component more easier to test.

### Misc
- some good tests here


### /TicketList

#### Must Have
- Use a functional approach rather than a class based approach. stay consistent with rest of code plus there's no need.

#### Nice To Have

### Misc
- not much to add, fair clear component
- some good tests here

### /Total

#### Must Have
- remove unused basket card import

#### Nice To Have
- if you create a container file with the redux logic to dispatch actions, then you can separate the redux logic away.
This also makes the component more easier to test.

### /Reducers

#### Must Have
- use the action types constants and replace the hard coded string
- strip the update logic in both action types into separate functions
- use destructuring when re-assigning the return values ``return {...action, quantity: item.quantity + 1};``

#### Nice To Have
- 

### Review

#### Must Have
- no need to return TicketList and basket in an array, just have the components underneath each other ``<div><TicketList/><div className="basket"></div></div>``
- Create another component called something like ``<BasketContainer/>`` and have the Basket and Total component inside.
That way you can import the Basket.scss class in there instead of the BasketCard.

#### Nice To Have
- some linting
